package org.selivnad.ts1;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.ErrorsCollector;
import com.codeborne.selenide.logevents.SelenideLogger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.Collections;

import static org.selivnad.ts1.TestResources.*;
import static org.selivnad.ts1.TestsConfig.BACKPACK_NAME;
import static org.selivnad.ts1.TestsConfig.HOODIE_NAME;
import static org.selivnad.ts1.TestsConfig.TEE_NAME;

public class WishlistTests {

    @BeforeAll
    public static void setUpAll() {
        Configuration.browserSize = "1280x1000";
        Configuration.browser = "chrome";
        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-blink-features=AutomationControlled");
        options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
        options.addArguments("--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36");

        Configuration.browserCapabilities.setCapability(ChromeOptions.CAPABILITY, options);

        SelenideLogger.addListener("allure", new ErrorsCollector());

        openHomePage();
        login("testtest@gmail.com", "Test123.");
    }

    @BeforeEach
    public void setUpEach() {
        openHomePage();
    }

    @AfterEach
    public void teardownEach() {
        clearWishlist();
    }

    // 1 - 2 - 16
    @Test
    void testWishlistOneItem() {
        addToWishlist(TEE_NAME);
        checkIsNotWishlistEmpty();
        checkWishlistContainsItem(TEE_NAME);
    }


    // 1 - 2 - 11 - 18 - 15
    @Test
    void testWishlistTwoItemsDeleteOne() {
        addToWishlist(TEE_NAME);
        openHomePage();
        addToWishlist(BACKPACK_NAME);
        removeFromWishlist(TEE_NAME);
        checkIsNotWishlistEmpty();
        checkWishlistContainsItem(BACKPACK_NAME);
    }

    // 1 - 2 - 10 - 14 - 18 - 14 - 9 - 13
    @Test
    void testWishlistAndRemoveTwiceThenAddAgain() {
        addToWishlist(TEE_NAME);
        removeFromWishlist(TEE_NAME);
        checkIsWishlistEmpty();
        openHomePage();
        addToWishlist(BACKPACK_NAME);
        removeFromWishlist(BACKPACK_NAME);
        checkIsWishlistEmpty();
        openHomePage();
        addToWishlist(HOODIE_NAME);
        checkIsNotWishlistEmpty();
        checkWishlistContainsItem(HOODIE_NAME);
    }

    // 1 - 2 - 11 - 9 - 13
    @Test
    void testWishlistThreeItems() {
        addToWishlist(TEE_NAME);
        openHomePage();
        addToWishlist(BACKPACK_NAME);
        openHomePage();
        addToWishlist(HOODIE_NAME);
        checkIsNotWishlistEmpty();
        checkWishlistContainsItem(TEE_NAME);
        checkWishlistContainsItem(BACKPACK_NAME);
        checkWishlistContainsItem(HOODIE_NAME);
    }

    // 1 - 2 - 11 - 12
    @Test
    void testWishlistTwoItems() {
        addToWishlist(TEE_NAME);
        openHomePage();
        addToWishlist(BACKPACK_NAME);
        checkIsNotWishlistEmpty();
        checkWishlistContainsItem(TEE_NAME);
        checkWishlistContainsItem(BACKPACK_NAME);
        checkWishlistDoesNotContainItem(HOODIE_NAME);
    }

    // 1 - 2 - 10 - 14 - 12
    @Test
    void testWishlistThenRemoveThenAdd() {
        addToWishlist(TEE_NAME);
        checkIsNotWishlistEmpty();
        checkWishlistContainsItem(TEE_NAME);
        removeFromWishlist(TEE_NAME);
        checkIsWishlistEmpty();
        openHomePage();
        addToWishlist(BACKPACK_NAME);
        checkIsNotWishlistEmpty();
        checkWishlistContainsItem(BACKPACK_NAME);
    }

    // 1 - 2 - 10 - 15
    @Test
    void testWishlistAndDeleteOneItem() {
        addToWishlist(TEE_NAME);
        removeFromWishlist(TEE_NAME);
        checkIsWishlistEmpty();
    }
}
