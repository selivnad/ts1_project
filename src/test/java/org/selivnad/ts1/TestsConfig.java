package org.selivnad.ts1;


public class TestsConfig {

    public static final String URL = "https://magento.softwaretestingboard.com/";
    public final static String TEE_NAME = "Radiant Tee";
    public final static String HOODIE_NAME = "Hero Hoodie";
    public final static String BACKPACK_NAME = "Fusion Backpack";
}
