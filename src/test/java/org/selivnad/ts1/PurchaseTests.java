package org.selivnad.ts1;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.ErrorsCollector;
import com.codeborne.selenide.logevents.SelenideLogger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.Collections;

import static com.codeborne.selenide.Selenide.sleep;
import static org.selivnad.ts1.TestResources.*;
import static org.selivnad.ts1.TestsConfig.BACKPACK_NAME;
import static org.selivnad.ts1.TestsConfig.HOODIE_NAME;
import static org.selivnad.ts1.TestsConfig.TEE_NAME;

class PurchaseTests {

    @BeforeAll
    public static void setUpAll() {
        Configuration.browserSize = "1280x1000";
        Configuration.browser = "chrome";
        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-blink-features=AutomationControlled");
        options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
        options.addArguments("--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36");

        Configuration.browserCapabilities.setCapability(ChromeOptions.CAPABILITY, options);

        SelenideLogger.addListener("allure", new ErrorsCollector());
    }

    @BeforeEach
    public void setUpEach() {
        openHomePage();
    }

    // 1 - 2 - 3 - 5 - 6
    @Test
    void testPurchaseOutOfStock() {
        String productName = TEE_NAME;
        openProductPage(productName);
        selectSize("M");
        selectSize("Blue");
        setProductQuantity(99999);
        addItemToCart();
        verifyOutOfStockMessage();
    }

    // 1 - 4 - 5 - 6
    @Test
    void testPurchaseOutOfStockFromMainPage() {
        String productName = TEE_NAME;
        selectSizeOnMainPage(productName, "M");
        selectColorOnMainPage(productName, "Blue");
        addToCartFromMainPage(productName);
        sleep(2000);
        openCart();
        setMiniCartQuantity(productName, 99999);
        clickUpdateQuantity();
        verifyOutOfStockModal();
    }

    // 1 - 4 - 5 - 7 - 8
    @Test
    void testPurchaseSuccess() {
        String productName = HOODIE_NAME;
        selectSizeOnMainPage(productName, "XS");
        selectColorOnMainPage(productName, "Gray");
        addToCartFromMainPage(productName);
        sleep(2000);
        openCart();
        clickProceedToCheckout();
        fillShippingForm("test@gmail.com", "Tester", "Test", "Test St.", "Testity", "102 00" ,"Czechia", "+420 123 123 123", true);
        clickPlaceOrder();
        checkOrderPlacementSuccess();
    }

    // 1 - 4 - 5 - 10 - 11 - 8
    @Test
    void testPurchaseSuccessWithDeliveryEstimation() {
        String productName = BACKPACK_NAME;
        addToCartFromMainPage(productName);
        sleep(2000);
        openFullCart();
        estimateDelivery("Czechia", "102 00");
        clickProceedToCheckout();
        fillShippingForm("test@gmail.com", "Tester", "Test", "Test St.", "Testity", "102 00" ,"Czechia", "+420 123 123 123", false);
        sleep(2000);
        clickPlaceOrder();
        checkOrderPlacementSuccess();
    }
}
