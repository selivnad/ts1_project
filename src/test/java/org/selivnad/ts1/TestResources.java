package org.selivnad.ts1;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.value;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.executeJavaScript;
import static com.codeborne.selenide.Selenide.open;
import static org.selivnad.ts1.TestsConfig.BACKPACK_NAME;
import static org.selivnad.ts1.TestsConfig.HOODIE_NAME;
import static org.selivnad.ts1.TestsConfig.TEE_NAME;

public class TestResources {

    public static void openHomePage() {
        open(TestsConfig.URL);
    }

    public static void login(String login, String password) {
        $$("a").findBy(text("Sign In")).click();
        $("input[name='login[username]']").setValue(login);
        $("input[name='login[password]']").setValue(password);
        $$("span").findBy(text("Sign In")).click();
    }

    public static void openProductPage(String productName) {
        $("a[title='" + productName + "']").click();
    }

    public static void selectSize(String size) {
        $("div[option-label='" + size + "']").click();
    }

    public static void selectSizeOnMainPage(String productName, String size) {
        $$(".product-item").findBy(text(productName)).hover();
        $$(".product-item").findBy(text(productName)).$$(".swatch-option.text").findBy(text(size)).click();
    }

    public static void selectColorOnMainPage(String productName, String color) {
        $$(".product-item").findBy(text(productName)).hover();
        $$(".product-item").findBy(text(productName)).$$(".swatch-option.color").findBy(attribute("aria-label", color)).click();
    }

    public static void addToCartFromMainPage(String productName) {
        $$(".product-item").findBy(text(productName)).hover();
        $$(".product-item").findBy(text(productName)).$$("button.tocart").findBy(text("Add to Cart")).click();
    }

    public static void addToWishlist(String productName) {
        $$(".product-item").findBy(text(productName)).hover();
        $$(".product-item").findBy(text(productName)).$("a.action.towishlist").click();
    }

    public static void removeFromWishlist(String productName) {
        $$(".product-item").findBy(text(productName)).hover();
        $$(".product-item").findBy(text(productName)).$("a.btn-remove.action.delete").click();
    }

    public static void selectColor(String color) {
        $("div[option-label='" + color + "']").click();
    }

    public static void addItemToCart() {
        $("button[title='Add to Cart']").click();
    }

    public static void verifyCartSuccessMessage(String productName) {
        $("div.message-success").shouldBe(visible);
        $("div.message-success").shouldHave(text("You added " + productName + " to your shopping cart"));
    }

    public static void verifyOutOfStockMessage() {
        $("div#qty-error.mage-error").shouldBe(visible);
    }

    public static void openCart() {
        $(".action.showcart").click();
    }

    public static void openFullCart() {
        $(".action.showcart").click();
        $$("span").findBy(text("View and Edit Cart")).click();
    }

    public static void setProductQuantity(int quantity) {
        $("#qty").setValue(String.valueOf(quantity));
        $("#qty").shouldHave(value(String.valueOf(quantity)));
    }

    public static void clickUpdateQuantity() {
        $("button[title='Update']").click();
    }

    public static void verifyOutOfStockModal() {
        $$(".modal-content").findBy(text("The requested qty exceeds the maximum qty allowed in shopping cart")).shouldBe(visible);
    }

    public static void setMiniCartQuantity(String productName, int quantity) {
        $$("div.product-item-details").findBy(text(productName)).hover();
        $$("div.product-item-details").findBy(text(productName))
                .$("input.cart-item-qty").setValue(String.valueOf(quantity));
        $$("div.product-item-details").findBy(text(productName))
                .$("input.cart-item-qty").shouldHave(value(String.valueOf(quantity)));
    }

    public static void clickProceedToCheckout() {
        SelenideElement button = $("button[title='Proceed to Checkout']");
        executeJavaScript("arguments[0].click();", button);
    }

    public static void clickPlaceOrder() {
        $("button[title='Place Order']").click();
    }

    public static void checkOrderPlacementSuccess() {
        $$("span.base").findBy(text("Thank you for your purchase!")).should(visible);
    }

    public static void fillShippingForm(String email, String firstName, String lastName, String street, String city,
                                        String postalCode, String country, String phone, boolean shouldChooseShipping) {
        $("select[name='country_id']").selectOptionContainingText(country);
        $("#customer-email").setValue(email);
        $("input[name='firstname']").setValue(firstName);
        $("input[name='lastname']").setValue(lastName);
        $("input[name='street[0]']").setValue(street);
        $("input[name='city']").setValue(city);
        $("input[name='postcode']").setValue(postalCode);
        $("input[name='telephone']").setValue(phone);

        if (shouldChooseShipping) {
            $$("input[name='ko_unique_3']").findBy(value("flatrate_flatrate")).click();
        }

        $("button[data-role='opc-continue']").click();
    }

    public static void estimateDelivery(String country, String postalCode) {
        $("strong#block-shipping-heading").click();
        $("select[name='country_id']").selectOptionContainingText(country);
        $("input[name='postcode']").setValue(postalCode);
        $$("span").findBy(text("Flat Rate - Fixed")).shouldBe(visible);
    }

    public static void openWishlist() {
        $("button[data-action='customer-menu-toggle']").click();
        $$("a").findBy(text("My Wish List")).click();
    }

    public static void checkIsWishlistEmpty() {
        $$("span").findBy(text("You have no items in your wish list.")).shouldBe(visible);
    }

    public static void checkIsNotWishlistEmpty() {
        $$("span").findBy(text("You have no items in your wish list.")).shouldNotBe(visible);
    }

    public static void checkWishlistContainsItem(String productName) {
        $$(".product-item").findBy(text(productName)).shouldBe(visible);
    }

    public static void checkWishlistDoesNotContainItem(String productName) {
        $$(".product-item").findBy(text(productName)).shouldNot(exist);
    }

    public static void clearWishlist() {
        openHomePage();
        openWishlist();
        if ($$(".product-item").findBy(text(TEE_NAME)).exists()) {
            removeFromWishlist(TEE_NAME);
        }
        if ($$(".product-item").findBy(text(BACKPACK_NAME)).exists()) {
            removeFromWishlist(BACKPACK_NAME);
        }
        if ($$(".product-item").findBy(text(HOODIE_NAME)).exists()) {
            removeFromWishlist(HOODIE_NAME);
        }
    }

    public static void logout() {
        $("button[data-action='customer-menu-toggle']").click();
        $$("a").findBy(text("Sign Out")).click();
    }
}
